    function makeABet(matchID, t1Score, t2Score) {
        document.forms['form']['matchID'].value = matchID;
        document.forms['form']['t1Score'].value = t1Score;
        document.forms['form']['t2Score'].value = t2Score;
        document.forms['form'].submit();
    }


    function deleteMatch(matchID) {
        if (confirm('Are you really sure about it?')){
            document.forms['form']['id'].value = matchID;
            document.forms['form'].action = '/en/admin/matches/ '+ matchID + '/delete';
            document.forms['form'].submit();
        }
    }

    function updateMatch(matchID, t1Score, t2Score) {
            if (confirm('Are you really sure about it?')){
                document.forms['form']['id'].value = matchID;
                document.forms['form']['t1Score'].value = t1Score;
                document.forms['form']['t2Score'].value = t2Score;
                document.forms['form'].action = '/en/admin/matches/ '+ matchID + '/update';
                document.forms['form'].submit();
            }
        }

         function createMatch() {
                    if (confirm('Are you really sure about it?')){
                        document.forms['form'].action = '/en/admin/matches/create';
                        document.forms['form'].submit();
                    }
                }
