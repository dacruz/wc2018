package com.dac.wc2018.services;

import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.model.Match;
import com.dac.wc2018.model.MatchesPerDay;
import com.dac.wc2018.model.UserBet;
import com.dac.wc2018.repositories.MatchRepository;
import com.dac.wc2018.repositories.UserBetRepository;
import com.dac.wc2018.services.exceptions.RecordNotFoundException;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service
public class MatchService {

    @Inject
    private MatchRepository matchRepository;

    @Inject
    private UserBetRepository userBetRepository;

    private DateTime minDate = DateTime.now().withMonthOfYear(6).withDayOfMonth(14);

    /**
     * @return
     */
    public List<MatchesPerDay> getUpcomingMatches() {
        DateTime refDate = new DateTime();
        if (DateTime.now().isBefore(this.minDate)) {
            refDate = this.minDate;
        }
        return getPerDay(this.matchRepository.findMatches(refDate, refDate.plusDays(3)));
    }


    /**
     * @return
     */
    public List<MatchesPerDay> getAllNextMatches() {
        DateTime refDate = new DateTime();
        if (DateTime.now().isBefore(this.minDate)) {
            refDate = this.minDate;
        }
        return getPerDay(this.matchRepository.findMatches(refDate.minusHours(2), refDate.plusDays(80)));
    }

    /**
     * @return
     */
    public List<MatchesPerDay> getAllPastMatches() {
        return getPerDay(this.matchRepository.findMatches(new DateTime().minusDays(60), new DateTime().minusHours(2)));
    }

    /**
     * @param id
     * @return
     */
    public Match findById(String id) {

        Match match = this.matchRepository.findOne(id);

        if (match == null) {
            throw new RecordNotFoundException();
        }

        return match;
    }

    /**
     * @param match
     * @return
     */
    public List<UserBet> getBets(Match match) {
        List<UserBet> ret = this.userBetRepository.findByMatchOrderByDateTimeAsc(match);
        ret.sort((a, b) -> {
            if (a.getPoints() > b.getPoints()) {
                return -1;
            } else {
                return 1;
            }
        });
        return ret;
    }

    /**
     * @return
     */
    public List<Match> getAllMatches() {
        return this.matchRepository.getAllMatches();
    }

    public ServiceResponse<Boolean> updateScore(String id, int t1Score, int t2Score) {
        ServiceResponse<Boolean> response = new ServiceResponse<>();
        response.setData(false);
        Match match = this.matchRepository.findOne(id);
        match.setT1Score(t1Score);
        match.setT2Score(t2Score);
        this.matchRepository.save(match);
        response.setData(true);
        return response;
    }

    public ServiceResponse<Boolean> createMatch(String t1, String t2, DateTime dateTime, String phase) {
        Match match = new Match();
        match.setT1(t1);
        match.setT2(t2);
        match.setPhase(phase);
        match.setDateTime(dateTime);
        this.matchRepository.save(match);
        return new ServiceResponse<>(true);
    }

    public ServiceResponse<Boolean> deleteMatch(String id) {
        this.matchRepository.delete(id);
        return new ServiceResponse<>(true);
    }

    private List<MatchesPerDay> getPerDay(List<Match> matches) {
        List<MatchesPerDay> result = new ArrayList<>();
        MatchesPerDay matchesPerDay = null;
        for (Match match : matches) {
            if (matchesPerDay == null || matchesPerDay.getDateTime().getDayOfYear() != match.getDateTime().getDayOfYear()) {
                matchesPerDay = new MatchesPerDay();
                matchesPerDay.setDateTime(match.getDateTime());
                matchesPerDay.getMatches().add(match);
                result.add(matchesPerDay);
            } else {
                matchesPerDay.getMatches().add(match);
            }
        }
        return result;
    }


}
