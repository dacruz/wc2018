package com.dac.wc2018.services.exceptions;

public class PasswordGenerationException extends RuntimeException {

    private static final long serialVersionUID = -3770950554660296055L;

    public PasswordGenerationException() {
    }

    public PasswordGenerationException(String arg0) {
        super(arg0);
    }

    public PasswordGenerationException(Throwable arg0) {
        super(arg0);
    }

    public PasswordGenerationException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public PasswordGenerationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
        super(arg0, arg1, arg2, arg3);
    }

}
