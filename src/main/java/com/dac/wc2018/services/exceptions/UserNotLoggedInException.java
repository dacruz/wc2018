package com.dac.wc2018.services.exceptions;

import com.dac.wc2018.core.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "You don' have a valid session")
public class UserNotLoggedInException extends ServiceException {

    public UserNotLoggedInException(String msg) {
        super(msg);
    }
}
