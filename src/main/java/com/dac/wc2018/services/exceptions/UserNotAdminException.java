package com.dac.wc2018.services.exceptions;

import com.dac.wc2018.core.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "You don't have enough rights to perform this action")
public class UserNotAdminException extends ServiceException {

    public UserNotAdminException(String msg) {
        super(msg);
    }
}
