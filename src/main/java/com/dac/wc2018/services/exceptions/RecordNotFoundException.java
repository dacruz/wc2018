package com.dac.wc2018.services.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Record not found. Sorry!")
public class RecordNotFoundException extends RuntimeException {
}
