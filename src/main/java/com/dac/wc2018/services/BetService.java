package com.dac.wc2018.services;

import com.dac.wc2018.core.Message;
import com.dac.wc2018.core.PointsCalculator;
import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.core.UserSession;
import com.dac.wc2018.model.*;
import com.dac.wc2018.repositories.MatchRepository;
import com.dac.wc2018.repositories.UserBetRepository;
import com.dac.wc2018.repositories.UserRepository;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BetService {


    @Inject
    private UserRepository userRepository;

    @Inject
    private UserBetRepository userBetRepository;

    @Inject
    private MatchRepository matchRepository;

    @Inject
    private UserSession session;


    /**
     * Get all user bets, including the ones not yet provided
     *
     * @return
     */
    public UserBetPanel getUserBetPanel() {

        //The panel
        UserBetPanel panel = new UserBetPanel();

        //Get all matches
        Iterable<Match> allMatches = this.matchRepository.findAll();

        //Get all user bets
        List<UserBet> allBets = this.userBetRepository.findByUser(this.session.getUser());

        //Iterate over all matches
        allMatches.forEach(match -> {
            UserBet bet = getUserBetForMatch(allBets, match);
            bet = safeBetCheck(match, bet);
            if (areBetsForTheMatchOpen(match)) {
                panel.getOpen().add(bet);
            } else {
                panel.getClosed().add(bet);
            }
        });

        orderBetsByMatchTime(panel);

        return panel;
    }

    public ServiceResponse<UserBetPanel> makeABet(String matchID, String t1Score, String t2Score) {

        ServiceResponse<UserBetPanel> response = new ServiceResponse<>();


        Match match = this.matchRepository.findOne(matchID);
        if (match == null) {
            response.getMessages().add(new Message("error", "Sorry, no match found this id"));
            response.setData(this.getUserBetPanel());
            return response;
        }

        if (!areBetsForTheMatchOpen(match)) {
            response.getMessages().add(new Message("error", "Sorry, bets for this match are closed. Don't try to shit with me!"));
            response.setData(this.getUserBetPanel());
            return response;
        }

        if (!(isInteger(t1Score) && Integer.parseInt(t1Score) >= 0)) {
            response.getMessages().add(new Message("error", "T1 Score must be a positive integer"));
            response.setData(this.getUserBetPanel());
        }

        if (!(isInteger(t2Score) && Integer.parseInt(t2Score) >= 0)) {
            response.getMessages().add(new Message("error", "T2 Score must be a positive integer"));
            response.setData(this.getUserBetPanel());
        }

        if (!response.getMessages().isEmpty()) {
            return response;
        }

        UserBet bet = this.userBetRepository.findByMatchIdAndUser(matchID, this.session.getUser());

        if (bet == null) {
            bet = new UserBet();
            bet.setUser(this.session.getUser());
            bet.setMatch(match);
        }

        bet.setDateTime(new DateTime());
        bet.setT1Score(Integer.parseInt(t1Score));
        bet.setT2Score(Integer.parseInt(t2Score));
        this.userBetRepository.save(bet);
        response.getMessages().add(new Message("success", "Your bet was registered. Good luck!"));

        response.setData(this.getUserBetPanel());
        return response;
    }

    /**
     * Calculate the result of the game and return it
     *
     * @return
     */
    public List<UserPoints> getResults() {

        //The result list
        List<UserPoints> result = new ArrayList<>();

        // Get all users
        Iterable<User> allUsers = this.userRepository.findAll();

        // Get all bets
        Iterable<UserBet> allBets = this.userBetRepository.findAll();

        allUsers.forEach(user -> {
            List<UserBet> allUserBets = getAllUserBets(user, allBets);
            result.add(new UserPoints(user, calculateNumberOfPoints(allUserBets), calculateNumberOfPoints24H(allUserBets), 0));
        });


        result.sort((userA, userB) -> {
            if (userA.getPoints() > userB.getPoints()) {
                return -1;
            } else if (userA.getPoints() < userB.getPoints()) {
                return 1;
            }
            return 0;
        });

        int ranking = 1;
        for (int index = 0; index < result.size(); index++) {
            UserPoints userPoints = result.get(index);
            if (index == 0) {
                userPoints.setRanking(ranking);
                ranking = ranking + 1;
            } else {
                UserPoints before = result.get(index - 1);
                if (userPoints.getPoints() == before.getPoints()) {
                    userPoints.setRanking(before.getRanking());
                } else {
                    userPoints.setRanking(ranking);
                    ranking = ranking + 1;
                }
            }
        }

        return result;
    }

    private int calculateNumberOfPoints(List<UserBet> userBets) {

        int points = 0;

        for (UserBet bet : userBets) {
            if (isMatchReadyToBeComputed(bet.getMatch())) {
                points = points + PointsCalculator.calculateNumberOfPoints(bet);
            }
        }

        return points;
    }

    private int calculateNumberOfPoints24H(List<UserBet> userBets) {

        int points = 0;

        for (UserBet bet : userBets) {
            if (isMatchReadyToBeComputed(bet.getMatch()) && isMatchInLast24H(bet.getMatch())) {
                points = points + PointsCalculator.calculateNumberOfPoints(bet);
            }
        }

        return points;
    }

    private boolean isMatchInLast24H(Match match) {
        return DateTime.now().minusDays(1).getDayOfYear() == match.getDateTime().getDayOfYear();
    }

    private boolean isMatchReadyToBeComputed(Match match) {
        return match.getT1Score() != null && match.getT2Score() != null;
    }

    private List<UserBet> getAllUserBets(User user, Iterable<UserBet> allBets) {
        List<UserBet> result = new ArrayList<>();
        allBets.forEach((UserBet bet) -> {
            if (bet.getUser().getId().equals(user.getId()))
                result.add(bet);
        });
        return result;
    }

    private UserBet getUserBetForMatch(List<UserBet> allBets, Match match) {
        UserBet result = null;
        for (UserBet bet : allBets) {
            if (bet.getMatch().getId().equals(match.getId())) {
                result = bet;
                break;
            }
        }
        return result;
    }

    private boolean areBetsForTheMatchOpen(Match match) {
        return match.getDateTime().getDayOfYear() > DateTime.now().getDayOfYear();
    }

    private UserBet safeBetCheck(Match match, UserBet bet) {
        if (bet == null) {
            bet = new UserBet();
            bet.setMatch(match);
            bet.setUser(this.session.getUser());
        }
        return bet;
    }

    private void orderBetsByMatchTime(UserBetPanel panel) {
        panel.getOpen().sort((b1, b2) -> {
            if (b1.getMatch().getDateTime().isBefore(b2.getMatch().getDateTime())) {
                return -1;
            } else {
                return 1;
            }
        });

        panel.getClosed().sort((b1, b2) -> {
            if (b1.getMatch().getDateTime().isBefore(b2.getMatch().getDateTime())) {
                return -1;
            } else {
                return 1;
            }
        });
    }

    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }


}
