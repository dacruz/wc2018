package com.dac.wc2018.services;

import com.dac.wc2018.core.Message;
import com.dac.wc2018.core.UserSession;
import com.dac.wc2018.services.exceptions.UserNotAdminException;
import com.dac.wc2018.services.exceptions.UserNotLoggedInException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.List;

@Component
public class DataValidationService {

    @Inject
    private UserSession userSession;

    void mandatory(final List<Message> messages, String data, String message) {
        if (StringUtils.isEmpty(data))
            messages.add(new Message("error", message));
    }


    public void validateSession() {
        //User must be logged in
        if (this.userSession.getUser() == null) {
            throw new UserNotLoggedInException("You must have a valid session to perform this operation");
        }
    }

    public void validateAdminSession() {
        this.validateSession();
        if (!this.userSession.getUser().isAdmin()) {
            throw new UserNotAdminException("You must be admin to perform this action.");
        }
    }
}
