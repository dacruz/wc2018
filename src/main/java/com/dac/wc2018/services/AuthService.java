package com.dac.wc2018.services;

import com.dac.wc2018.core.Message;
import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.core.UserSession;
import com.dac.wc2018.model.User;
import com.dac.wc2018.repositories.UserRepository;
import com.dac.wc2018.services.exceptions.PasswordGenerationException;
import com.dac.wc2018.services.util.PasswordUtil;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;

/**
 * Created by Jose on 28/03/2017.
 */
@Service
@Transactional
public class AuthService {

    @Inject
    private UserRepository userRepository;

    @Inject
    private DataValidationService validation;

    @Inject
    private PasswordUtil passwordUtil;

    @Inject
    private UserSession userSession;

    /**
     * Performs the login
     *
     * @param email
     * @param pwd
     * @return
     * @throws PasswordGenerationException
     */
    public ServiceResponse<Boolean> login(String email, String pwd) throws PasswordGenerationException {
        ServiceResponse<Boolean> response = new ServiceResponse<>();
        response.setData(false);//Default is to prevent login
        this.validation.mandatory(response.getMessages(), email, "E-mail is mandatory");
        this.validation.mandatory(response.getMessages(), pwd, "Password is mandatory");
        if (!response.getMessages().isEmpty())
            return response;

        // So far so good
        User user = this.userRepository.findByEmail(email);
        if (user == null) {
            return returnInvalidLoginResponse(response);
        }

        // So far so good
        String pwdProvided = this.passwordUtil.generateStrongPasswordHash(pwd, user.getSalt());
        if (!pwdProvided.equals(user.getPwd())) {
            return returnInvalidLoginResponse(response);
        }

        //Good
        this.userSession.setUser(user);
        response.setData(true);
        return response;
    }


    /**
     * Logout the user
     */
    public void logout() {
        this.userSession.setUser(null);
    }


    /**
     * Updates the user password
     *
     * @param pwd
     * @param newPWD
     * @param newPWDConfirmation
     * @return
     * @throws PasswordGenerationException
     */
    public ServiceResponse<Boolean> updatePassword(String pwd, String newPWD, String newPWDConfirmation) throws
            PasswordGenerationException {

        // Check session
        this.validation.validateSession();

        ServiceResponse<Boolean> response = new ServiceResponse<>();
        response.setData(false);

        this.validation.mandatory(response.getMessages(), pwd, "Current password is mandatory");
        this.validation.mandatory(response.getMessages(), newPWD, "New password is mandatory");
        this.validation.mandatory(response.getMessages(), newPWDConfirmation, "New password confirmation is mandatory");
        if (!response.getMessages().isEmpty())
            return response;

        User user = this.userSession.getUser();

        String pwdProvided = this.passwordUtil.generateStrongPasswordHash(pwd, user.getSalt());
        if (!pwdProvided.equals(user.getPwd())) {
            response.getMessages().add(new Message("error", "Invalid current password"));
            return response;
        }

        if (!newPWD.equals(newPWDConfirmation)) {
            response.getMessages().add(new Message("error", "New password & new password confirmation must have the same value"));
            return response;
        }

        if (!this.passwordUtil.isValidPassword(newPWD)) {
            response.getMessages().add(new Message("error", "New password does not match the rules. Password >= 6 & Password <= 15 characters"));
            return response;
        }

        // Update the password
        user.setPwd(this.passwordUtil.generateStrongPasswordHash(newPWD, user.getSalt()));
        this.userRepository.save(user);
        response.setData(true);

        return response;
    }


    private ServiceResponse<Boolean> returnInvalidLoginResponse(ServiceResponse<Boolean> response) {
        response.getMessages().add(new Message("error", "Invalid e-mail or password"));
        return response;
    }


}
