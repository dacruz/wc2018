/**
 *
 */
package com.dac.wc2018.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Mr. DAC on 15 avr. 2017
 */
@Service
@Transactional
@Slf4j
public class EventPlanningService {

//    @Autowired
//    private EventPlanRepository eventPlanRepository;
//
//    @Autowired
//    private ServiceDataValidation validation;
//
//    public EventPlan create(String name, String notes, String[] dates) throws ServiceException {
//
//        List<FunctionalError> errors = new ArrayList<>();
//
//        this.validation.mandatory(errors, name, LabelCode.EVENT_NAME);
//
//        if (dates == null || dates.length < 2) {
//            this.validation.addError(errors, ErrorCode.EVENT_INVALID_NUMBER_DATES);
//        }
//
//		/* Those are the mandatory data, so stop here in case of errors */
//        if (!errors.isEmpty()) {
//            throw new ServiceException(errors);
//        }
//
//        //Create my event
//        EventPlan eventPlan = new EventPlan();
//        eventPlan.setTitle(name);
//        eventPlan.setNote(notes);
//
//        for (String strDate : dates) {
//            EventPlan.DateProposal dateTimeProposal = new EventPlan.DateProposal();
//            dateTimeProposal.setOwner(eventPlan);
//            dateTimeProposal.setDate(LocalDate.parse(strDate, DateTimeFormat.forPattern("MM/dd/YYYY")));
//            eventPlan.getProposals().add(dateTimeProposal);
//        }
//
//        //Save the event
//        this.eventPlanRepository.save(eventPlan);
//
//        log.debug("New EventPlan created [{}]", eventPlan.getTitle());
//
//        return eventPlan;
//    }
//
//    public EventPlan getByInternalID(String internalID) {
//        EventPlan event = this.eventPlanRepository.findByInternalID(internalID);
//        if (event == null) {
//            throw new ServiceException(ErrorCode.INVALID_PASSWORD);
//        }
//        return event;
//    }
}
