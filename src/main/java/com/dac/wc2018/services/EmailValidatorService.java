package com.dac.wc2018.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Created by Jose on 11/04/2017.
 */
@Component
public class EmailValidatorService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public boolean isValidEmailAddress(String email) {

        if (StringUtils.isEmpty(email))

            return false;

        boolean result = true;
        try {
            new InternetAddress(email).validate();
        } catch (AddressException ex) {
            this.logger.debug("Invalid e-mail address", ex);
            result = false;
        }

        return result;
    }

}
