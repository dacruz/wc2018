package com.dac.wc2018.services;

import com.dac.wc2018.core.Message;
import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.model.User;
import com.dac.wc2018.repositories.UserRepository;
import com.dac.wc2018.services.util.MD5Helper;
import com.dac.wc2018.services.util.PasswordUtil;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;

@Service
@Transactional
public class UserRegistrationService {


    @Inject
    private DataValidationService validation;

    @Inject
    private UserRepository userRepository;

    @Inject
    private PasswordUtil passwordUtil;

    @Inject
    private MD5Helper md5Helper;

    public ServiceResponse<Boolean> register(String firstName, String lastName, String email, String pwd, boolean admin) throws Exception {

        ServiceResponse<Boolean> response = new ServiceResponse<>();
        response.setData(false);
        this.validation.mandatory(response.getMessages(), firstName, "First name is mandatory");
        this.validation.mandatory(response.getMessages(), lastName, "Last name is mandatory");
        this.validation.mandatory(response.getMessages(), email, "E-mail is mandatory");
        this.validation.mandatory(response.getMessages(), pwd, "Password is mandatory");

        if (!response.getMessages().isEmpty())
            return response;

		/* Remaining validation, such as user already registered */

        User user = this.userRepository.findByEmail(email);
        if (user != null) {
            response.getMessages().add(new Message("error", "This e-mail is already in use"));
            return response;
        }

        user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAdmin(admin);
        user.setEmail(email);
        user.setGravatarHash(this.md5Helper.MD5(user.getEmail()));
        user.setSalt(this.passwordUtil.getSalt());
        user.setPwd(this.passwordUtil.generateStrongPasswordHash(pwd, user.getSalt()));
        user.setDateTimeCreation(new DateTime());

		/* save user data */
        this.userRepository.save(user);

        response.setData(true);
        return response;

    }

}
