package com.dac.wc2018.services;

import com.dac.wc2018.core.Phases;
import com.dac.wc2018.core.Teams;
import com.dac.wc2018.model.*;
import com.dac.wc2018.repositories.MatchRepository;
import com.dac.wc2018.repositories.UserBetRepository;
import com.dac.wc2018.repositories.UserRepository;
import com.dac.wc2018.services.exceptions.PasswordGenerationException;
import com.dac.wc2018.services.util.MD5Helper;
import com.dac.wc2018.services.util.PasswordUtil;
import org.joda.time.DateTime;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author Jose
 */
@Service
public class CreateMinData {

    private List<Team> teams = new ArrayList<>(Teams.teams.values());

    private List<Phase> phases = new ArrayList<>(Phases.phases.values());

    private List<Integer> goals = Arrays.asList(0, 1, 2, 3, 4);

    @Inject
    private UserRepository userRepository;

    @Inject
    private MatchRepository matchRepository;

    @Inject
    private UserBetRepository userBetRepository;

    @Inject
    private PasswordUtil passwordUtil;

    @Inject
    private MD5Helper md5Helper;

    @EventListener(ApplicationReadyEvent.class)
    public void createAdminUser() throws NoSuchAlgorithmException, PasswordGenerationException {
        User admin = this.userRepository.findByEmail("josevicentecruz@gmail.com");
        if (admin == null) {
            admin = new User();
            admin.setDateTimeCreation(new DateTime());
            admin.setAdmin(true);
            admin.setEmail("josevicentecruz@gmail.com");
            admin.setLastName("Cruz");
            admin.setFirstName("Jose");
            admin.setSalt(this.passwordUtil.getSalt());
            admin.setPwd(this.passwordUtil.generateStrongPasswordHash("aaaaaa", admin.getSalt()));
            admin.setGravatarHash(this.md5Helper.MD5(admin.getEmail()));
            admin.setBlocked(false);
            this.userRepository.save(admin);
        }
    }

    // @EventListener(ApplicationReadyEvent.class)
    public void create() throws NoSuchAlgorithmException, PasswordGenerationException {

        //Pick random elements from list
        Random rand = new Random();

        //Create users
        for (int index = 0; index < 20; index++) {
            User user = new User();
            user.setDateTimeCreation(new DateTime());
            user.setEmail("a" + index + "@gmail.com");
            user.setGravatarHash(this.md5Helper.MD5(user.getEmail()));
            user.setFirstName("User " + index);
            user.setLastName("Cruz");
            user.setBlocked(false);
            user.setSalt(this.passwordUtil.getSalt());
            user.setPwd(this.passwordUtil.generateStrongPasswordHash("aaaaaa", user.getSalt()));
            this.userRepository.save(user);
        }

        //Create matches
        for (int day = 0; day < 30; day++) {
            DateTime theDay = new DateTime().minusDays(10).plusDays(day).withHourOfDay(14).withMinuteOfHour(0);
            for (int matchNumber = 0; matchNumber < 2; matchNumber++) {
                Match match = new Match();
                match.setDateTime(theDay);
                match.setPhase(this.phases.get(rand.nextInt(this.phases.size())).getCode());
                match.setT1(this.teams.get(rand.nextInt(this.teams.size())).getCode());
                match.setT2(this.teams.get(rand.nextInt(this.teams.size())).getCode());
                if (theDay.isBefore(DateTime.now())) {
                    match.setT1Score(this.goals.get(rand.nextInt(this.goals.size())));
                    match.setT2Score(this.goals.get(rand.nextInt(this.goals.size())));
                }
                this.matchRepository.save(match);
            }
        }

        //Create bets
        this.matchRepository.findAll().forEach((Match match) -> {
            this.userRepository.findAll().forEach((User user) -> {
                if (!user.getFirstName().equals("User 1")) {
                    UserBet bet = new UserBet();
                    bet.setT1Score(this.goals.get(rand.nextInt(this.goals.size())));
                    bet.setT2Score(this.goals.get(rand.nextInt(this.goals.size())));
                    bet.setUser(user);
                    bet.setMatch(match);
                    bet.setDateTime(new DateTime());
                    this.userBetRepository.save(bet);
                }
            });
        });
    }


}
