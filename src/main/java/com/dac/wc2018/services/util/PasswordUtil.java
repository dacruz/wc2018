package com.dac.wc2018.services.util;

import com.dac.wc2018.services.exceptions.PasswordGenerationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created by Jose on 11/04/2017.
 */

@Component
public class PasswordUtil {

    private static String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            String myFormat = "%0" + paddingLength + "d";
            return String.format(myFormat, 0) + hex;
        } else {
            return hex;
        }
    }

    public boolean isValidPassword(String pwd) {
        return !StringUtils.isEmpty(pwd) && pwd.trim().length() >= 6 && pwd.trim().length() <= 15;
    }

    public String generateStrongPasswordHash(String password, byte[] salt) throws PasswordGenerationException {
        try {
            int iterations = 1000;
            char[] chars = password.toCharArray();

            PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return iterations + ":" + toHex(salt) + ":" + toHex(hash);

        } catch (Exception e) {
            throw new PasswordGenerationException(e);
        }
    }

    public byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
}
