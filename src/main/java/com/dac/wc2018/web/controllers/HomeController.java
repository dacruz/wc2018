package com.dac.wc2018.web.controllers;

import com.dac.wc2018.core.Layout;
import com.dac.wc2018.services.BetService;
import com.dac.wc2018.services.MatchService;
import com.dac.wc2018.web.forms.HomeForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;

@Controller
public class HomeController {

    @Inject
    private BetService betService;

    @Inject
    private MatchService matchService;

    @Inject
    private ControllerUtil util;

    @RequestMapping(value = {"/", ""})

    public String redirect() {
        return this.util.redirect("/home");
    }

    @RequestMapping(value = {"*/home"})
    @Layout(value = "layouts/default")
    public String home(HomeForm form) {
        form.setClassification(this.betService.getResults());
        form.setNextMatches(this.matchService.getUpcomingMatches());
        return "pages/home";
    }

    @RequestMapping(value = {"*/rules"})
    @Layout(value = "layouts/default")
    public String rules() {
        return "pages/rules";
    }


}