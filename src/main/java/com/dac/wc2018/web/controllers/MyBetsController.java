package com.dac.wc2018.web.controllers;

import com.dac.wc2018.core.Layout;
import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.model.UserBetPanel;
import com.dac.wc2018.services.BetService;
import com.dac.wc2018.web.forms.MyBetsForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;

@Controller
public class MyBetsController {

    @Inject
    private BetService betService;

    @RequestMapping(value = {"*/sec/mybets"}, method = RequestMethod.GET)
    @Layout(value = "layouts/default")
    public String displayMyBets(MyBetsForm form) {
        form.setPanel(this.betService.getUserBetPanel());
        return "pages/mybets";
    }

    @RequestMapping(value = {"*/sec/mybets"}, method = RequestMethod.POST)
    @Layout(value = "layouts/default")
    public String makeABet(MyBetsForm form) {
        ServiceResponse<UserBetPanel> response = this.betService.makeABet(form.getMatchID(), form.getT1Score(), form.getT2Score());
        form.setPanel(response.getData());
        form.getMessages().addAll(response.getMessages());
        return "pages/mybets";
    }

}