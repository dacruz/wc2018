package com.dac.wc2018.web.controllers;

import com.dac.wc2018.core.Layout;
import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.services.AuthService;
import com.dac.wc2018.web.forms.ChangePasswordForm;
import com.dac.wc2018.web.forms.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;

@Controller
public class AuthController {

    @Inject
    private AuthService authService;

    @Inject
    private ControllerUtil util;

    @RequestMapping(value = {"*/login"})
    @Layout(value = "layouts/default")
    public String login(LoginForm loginForm) {

        return "pages/login";
        
    }

    @RequestMapping(value = {"*/authenticate"})
    @Layout(value = "layouts/default")
    public String authenticate(LoginForm loginForm) throws Exception {

        ServiceResponse<Boolean> response = this.authService.login(loginForm.getEmail(),
                loginForm.getPwd());

        if (response.getData()) {
            return this.util.redirect("/home");
        }
        loginForm.getMessages().addAll(response.getMessages());

        return "pages/login";
    }

    @RequestMapping(value = {"*/logout"})
    @Layout(value = "layouts/default")
    public String logout() throws Exception {

        this.authService.logout();

        return this.util.redirect("/home");
    }

    @RequestMapping(value = {"*/changepassword"}, method = RequestMethod.GET)
    @Layout(value = "layouts/default")
    public String displayPasswordChangePage(ChangePasswordForm form) {

        return "pages/changepassword";

    }

    @RequestMapping(value = {"*/changepassword"}, method = RequestMethod.POST)
    @Layout(value = "layouts/default")
    public String updatePassword(ChangePasswordForm form) {

        ServiceResponse<Boolean> response = this.authService.updatePassword(form.getCurrentPassword(),
                form.getNewPassword(),
                form.getNewPasswordConfirmation());

        if (response.getData()) {
            return this.util.redirect("/home");
        }

        form.getMessages().addAll(response.getMessages());

        return "pages/changepassword";
    }


}
