package com.dac.wc2018.web.controllers;

import com.dac.wc2018.core.Layout;
import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.model.Match;
import com.dac.wc2018.services.MatchService;
import com.dac.wc2018.web.forms.MatchAdminForm;
import com.dac.wc2018.web.forms.MatchForm;
import com.dac.wc2018.web.forms.MatchesForm;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;

@Controller
public class MatchController {

    @Inject
    private MatchService matchService;

    @Inject
    private ControllerUtil util;

    @RequestMapping(value = {"*/matches"}, method = RequestMethod.GET)
    @Layout(value = "layouts/default")
    public String home(MatchesForm form) {
        form.setNextMatches(this.matchService.getAllNextMatches());
        form.setPastMatches(this.matchService.getAllPastMatches());
        return "pages/matches";
    }

    @RequestMapping(value = {"*/matches/{id}"}, method = RequestMethod.GET)
    @Layout(value = "layouts/default")
    public String view(@PathVariable(value = "id") final String id, MatchForm form) {
        Match match = this.matchService.findById(id);
        form.setMatch(match);
        form.getBets().addAll(this.matchService.getBets(form.getMatch()));
        return "pages/match";
    }

    @RequestMapping(value = {"*/admin/matches"}, method = RequestMethod.GET)
    @Layout(value = "layouts/default")
    public String matchAdminPage(MatchAdminForm form) {
        form.setMatches(this.matchService.getAllMatches());
        return "pages/matches-admin";
    }

    @RequestMapping(value = {"*/admin/matches/{id}/delete"}, method = RequestMethod.POST)
    @Layout(value = "layouts/default")
    public String deleteMatch(MatchAdminForm form) {
        ServiceResponse<Boolean> response = this.matchService.deleteMatch(form.getId());
        form.getMessages().addAll(response.getMessages());
        return this.util.redirect("/admin/matches");
    }

    @RequestMapping(value = {"*/admin/matches/{id}/update"}, method = RequestMethod.POST)
    @Layout(value = "layouts/default")
    public String updateMatch(@PathVariable(value = "id") final String id, MatchAdminForm form) {
        ServiceResponse<Boolean> response = this.matchService.updateScore(form.getId(), Integer.parseInt(form.getT1Score()), Integer.parseInt(form.getT2Score
                ()));
        form.getMessages().addAll(response.getMessages());
        return this.util.redirect("/admin/matches");
    }

    @RequestMapping(value = {"*/admin/matches/create"}, method = RequestMethod.POST)
    @Layout(value = "layouts/default")
    public String create(MatchAdminForm form) {
        ServiceResponse<Boolean> response = this.matchService.createMatch(form.getTeam1(), form.getTeam2(), DateTime.parse(form.getDateTime()), form.getPhase
                ());
        form.getMessages().addAll(response.getMessages());
        return this.util.redirect("/admin/matches");
    }

}