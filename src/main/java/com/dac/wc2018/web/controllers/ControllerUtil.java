package com.dac.wc2018.web.controllers;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ControllerUtil {

    public String redirect(String path) {
        return "redirect:/" + LocaleContextHolder.getLocale().getLanguage() + path;
    }
}
