package com.dac.wc2018.web.controllers;

import com.dac.wc2018.core.Layout;
import com.dac.wc2018.core.Message;
import com.dac.wc2018.core.ServiceResponse;
import com.dac.wc2018.services.UserRegistrationService;
import com.dac.wc2018.web.forms.CreateUserForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;

@Controller
public class UserController {

    @Inject
    private UserRegistrationService userRegistrationService;

    @RequestMapping(value = {"*/admin/createuser"}, method = RequestMethod.GET)
    @Layout(value = "layouts/default")
    public String displayCreateUserPage(CreateUserForm form) {
        return "pages/createuser";
    }

    @RequestMapping(value = {"*/admin/createuser"}, method = RequestMethod.POST)
    @Layout(value = "layouts/default")
    public String createUser(CreateUserForm form) throws Exception {
        ServiceResponse<Boolean> response = this.userRegistrationService.register(form.getFirstName(), form.getLastName(), form.getEmail(), form.getPwd(),
                form.isAdmin());
        if (!response.getData()) {
            form.getMessages().addAll(response.getMessages());
        } else {
            form.getMessages().add(new Message("success", "The user was created in the system."));
        }
        return "pages/createuser";
    }

}