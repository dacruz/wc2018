package com.dac.wc2018.web.forms;

import lombok.Data;

@Data
public class LoginForm extends BasicForm {

    private String email;

    private String pwd;

}
