package com.dac.wc2018.web.forms;

import com.dac.wc2018.model.MatchesPerDay;
import lombok.Data;

import java.util.List;

@Data
public class MatchesForm extends BasicForm {

    private List<MatchesPerDay> nextMatches;

    private List<MatchesPerDay> pastMatches;

}
