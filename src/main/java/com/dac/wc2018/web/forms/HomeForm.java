package com.dac.wc2018.web.forms;

import com.dac.wc2018.model.MatchesPerDay;
import com.dac.wc2018.model.UserPoints;
import lombok.Data;

import java.util.List;

@Data
public class HomeForm extends BasicForm {

    private List<UserPoints> classification;

    private List<MatchesPerDay> nextMatches;

}
