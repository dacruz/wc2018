package com.dac.wc2018.web.forms;

import com.dac.wc2018.model.Match;
import com.dac.wc2018.model.UserBet;
import lombok.Data;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

@Data
public class MatchForm extends BasicForm {

    private Match match;

    private List<UserBet> bets = new ArrayList<>();

    public boolean canDisclosure() {
        return this.match.getDateTime().getDayOfYear() <= DateTime.now().getDayOfYear();
    }
}
