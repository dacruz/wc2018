package com.dac.wc2018.web.forms;

import lombok.Data;

@Data
public class CreateUserForm extends BasicForm {

    private String email;

    private String pwd;

    private String firstName;

    private String lastName;

    private boolean admin;

}
