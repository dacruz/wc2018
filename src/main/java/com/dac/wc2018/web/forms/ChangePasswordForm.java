package com.dac.wc2018.web.forms;

import lombok.Data;

@Data
public class ChangePasswordForm extends BasicForm {

    private String currentPassword;
    private String newPassword;
    private String newPasswordConfirmation;

}
