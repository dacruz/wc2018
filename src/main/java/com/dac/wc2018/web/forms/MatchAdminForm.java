package com.dac.wc2018.web.forms;

import com.dac.wc2018.core.Phases;
import com.dac.wc2018.core.Teams;
import com.dac.wc2018.model.Match;
import com.dac.wc2018.model.Phase;
import com.dac.wc2018.model.Team;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MatchAdminForm extends BasicForm {

    private List<Match> matches;

    private List<Team> teams = new ArrayList<>(Teams.teams.values());

    private List<Phase> phases = new ArrayList<>(Phases.phases.values());

    private String id;

    private String team1;

    private String team2;

    private String t1Score;

    private String t2Score;

    private String phase;

    private String dateTime;
}
