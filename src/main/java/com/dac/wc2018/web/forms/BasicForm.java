package com.dac.wc2018.web.forms;

import com.dac.wc2018.core.Message;

import java.util.ArrayList;
import java.util.List;


public class BasicForm {

    private List<Message> messages;

    public List<Message> getMessages() {
        if (this.messages == null) {
            this.messages = new ArrayList<>();
        }
        return this.messages;
    }


}
