package com.dac.wc2018.web.forms;

import com.dac.wc2018.model.UserBetPanel;
import lombok.Data;

@Data
public class MyBetsForm extends BasicForm {
    private String matchID;
    private String t1Score;
    private String t2Score;
    private UserBetPanel panel;
}
