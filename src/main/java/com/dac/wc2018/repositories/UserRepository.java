package com.dac.wc2018.repositories;

import com.dac.wc2018.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Jose
 */
public interface UserRepository extends CrudRepository<User, String> {

    User findByEmail(String email);
}
