package com.dac.wc2018.repositories;

import com.dac.wc2018.model.Match;
import com.dac.wc2018.model.User;
import com.dac.wc2018.model.UserBet;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Jose
 */
public interface UserBetRepository extends CrudRepository<UserBet, String> {


    public UserBet findByMatchIdAndUser(String matchID, User user);

    public List<UserBet> findByMatchOrderByDateTimeAsc(Match match);

    public List<UserBet> findByUser(User user);

}
