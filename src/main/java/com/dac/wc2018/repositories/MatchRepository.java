package com.dac.wc2018.repositories;

import com.dac.wc2018.model.Match;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Jose
 */
public interface MatchRepository extends CrudRepository<Match, String> {

    @Query(value = "SELECT m FROM Match m WHERE m.dateTime >= ?1 and m.dateTime <= ?2 order by m.dateTime asc")
    public List<Match> findMatches(DateTime startDateTime, DateTime endDateTime);

    @Query(value = "SELECT m FROM Match m order by m.dateTime asc")
    public List<Match> getAllMatches();

}
