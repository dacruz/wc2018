package com.dac.wc2018.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class User {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String email;

    @NotNull
    private DateTime dateTimeCreation;

    @NotNull
    private boolean isBlocked;

    @NotNull
    private boolean isAdmin;

    @NotNull
    private byte[] salt;

    @NotNull
    private String pwd;

    @NotNull
    private String gravatarHash;

    private DateTime lastLogin;
}
