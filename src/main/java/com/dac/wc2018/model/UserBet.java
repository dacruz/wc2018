package com.dac.wc2018.model;

import com.dac.wc2018.core.PointsCalculator;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Data
public class UserBet {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private User user;
    @OneToOne(fetch = FetchType.EAGER)
    @NotNull
    private Match match;
    @NotNull
    private Integer t1Score;
    @NotNull
    private Integer t2Score;
    @NotNull
    private DateTime dateTime;

    public String getDateTimeAsString() {
        return this.dateTime.toString("dd-MMM HH:mm");
    }

    public boolean wasMadeByUser() {
        return this.dateTime != null && this.t1Score != null & this.t2Score != null && this.user != null;
    }

    public int getPoints() {
        return PointsCalculator.calculateNumberOfPoints(this);
    }

    public boolean isGoodWinner() {
        // Check for victory of t1
        return (this.getMatch().getT1Score() > this.getMatch().getT2Score() && this.getT1Score() > this.getT2Score()) ||
                // Check for victory of t2
                (this.getMatch().getT2Score() > this.getMatch().getT1Score() && this.getT2Score() > this.getT1Score());
    }

    public String getCSSName() {
        int points = this.getPoints();
        if (points == 4) {
            return "bg-success text-white";
        }
        if (points == 3) {
            return "bg-primary text-white";
        }
        if (points == 2) {
            return "bg-warning text-dark";
        }
        if (points == 1) {
            return "bg-info text-white";
        }
        return "bg-secondary text-white";
    }
}
