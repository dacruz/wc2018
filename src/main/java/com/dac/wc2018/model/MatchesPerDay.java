package com.dac.wc2018.model;

import lombok.Data;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

@Data
public class MatchesPerDay {
    private List<Match> matches = new ArrayList<>();
    private DateTime dateTime;

    public String getDateAsString() {
        return this.dateTime.toString("EEE, d MMM yyyy");
    }

}
