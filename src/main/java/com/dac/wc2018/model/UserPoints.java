package com.dac.wc2018.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserPoints {
    private User user;
    private int points;
    private int pointsLast24H;
    private int ranking;
}
