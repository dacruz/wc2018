package com.dac.wc2018.model;

import com.dac.wc2018.core.Phases;
import com.dac.wc2018.core.Teams;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tb_match")
public class Match {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @NotNull
    private String t1;

    @NotNull
    private String t2;

    private Integer t1Score;

    private Integer t2Score;

    @NotNull
    private DateTime dateTime;

    @NotNull
    private String phase;

    public String getDateTimeAsString() {
        return this.dateTime.toString("dd-MMM HH:mm");
    }

    public String getTimeAsString() {
        return this.dateTime.toString("HH:mm");
    }

    public String getT1Name() {
        return Teams.getName(this.getT1());
    }

    public String getT2Name() {
        return Teams.getName(this.getT2());
    }

    public String getPhaseName() {
        return Phases.getName(this.getPhase());
    }


}
