package com.dac.wc2018.model;

import lombok.Data;

@Data
public class Team {

    private String code;
    private String name;

    public Team() {

    }

    public Team(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
