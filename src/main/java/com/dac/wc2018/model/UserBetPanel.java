package com.dac.wc2018.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserBetPanel {
    private List<UserBet> open = new ArrayList<>();
    private List<UserBet> closed = new ArrayList<>();
}
