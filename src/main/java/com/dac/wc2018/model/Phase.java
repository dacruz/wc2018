package com.dac.wc2018.model;

import lombok.Data;

@Data
public class Phase {

    private String code;
    private String name;

    public Phase() {

    }

    public Phase(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
