package com.dac.wc2018.core;

import com.dac.wc2018.model.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Teams {

    public static HashMap<String, Team> teams = loadTeams();

    public static String getName(String code) {
        return teams.get(code).getName();
    }

    public static HashMap<String, Team> loadTeams() {
        List<Team> list = new ArrayList<>();
        list.add(new Team("ar", "Argentina"));
        list.add(new Team("au", "Australia"));
        list.add(new Team("be", "Belgium"));
        list.add(new Team("br", "Brazil"));
        list.add(new Team("co", "Colombia"));
        list.add(new Team("cr", "Costa Rica"));
        list.add(new Team("hr", "Croatia"));
        list.add(new Team("dk", "Denmark"));
        list.add(new Team("eg", "Egypt"));
        list.add(new Team("gb", "England"));
        list.add(new Team("fr", "France"));
        list.add(new Team("de", "Germany"));
        list.add(new Team("is", "Iceland"));
        list.add(new Team("ir", "IR Iran"));
        list.add(new Team("jp", "Japan"));
        list.add(new Team("kr", "Korea Republic"));
        list.add(new Team("mx", "Mexico"));
        list.add(new Team("ma", "Morocco"));
        list.add(new Team("ng", "Nigeria"));
        list.add(new Team("pa", "Panama"));
        list.add(new Team("pe", "Peru"));
        list.add(new Team("pl", "Poland"));
        list.add(new Team("pt", "Portugal"));
        list.add(new Team("ru", "Russia"));
        list.add(new Team("sa", "Saudi Arabia"));
        list.add(new Team("sn", "Senegal"));
        list.add(new Team("rs", "Serbia"));
        list.add(new Team("es", "Spain"));
        list.add(new Team("se", "Sweden"));
        list.add(new Team("ch", "Switzerland"));
        list.add(new Team("tn", "Tunisia"));
        list.add(new Team("uy", "Uruguay"));
        HashMap<String, Team> map = new HashMap<>();
        list.forEach(team -> map.put(team.getCode(), team));
        return map;
    }
}
