package com.dac.wc2018.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by Jose on 31/03/2017.
 */
public class PathLocaleChangeInterceptor extends HandlerInterceptorAdapter {

    private static final String DEFAULT_LANGUAGE = "en";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final List<String> supportedLanguages = Arrays.asList("pt", "fr", "en");
    private boolean languageTagCompliant = false;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String locale = getLocaleFromURL(request);

        if (!isLanguageSupported(locale)) {
            locale = DEFAULT_LANGUAGE;
        }

        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        if (localeResolver == null)
            throw new IllegalStateException("No LocaleResolver found: not in a DispatcherServlet request?");
        try {
            Locale.getAvailableLocales();
            localeResolver.setLocale(request, response, parseLocaleValue(locale));
        } catch (Exception ex) {
            this.logger.warn("Ignoring invalid locale value {}: " + ex.getMessage(), locale);
            this.logger.debug("Invalid locale", ex);
        }

        // Only possible response
        return true;

    }

    public boolean isLanguageTagCompliant() {
        return this.languageTagCompliant;
    }

    private Locale parseLocaleValue(String locale) {
        return (isLanguageTagCompliant() ? Locale.forLanguageTag(locale) : StringUtils.parseLocaleString(locale));
    }

    private boolean isLanguageSupported(String locale) {
        return locale != null && locale.trim().length() > 0 && this.supportedLanguages.contains(locale);
    }

    private String getLocaleFromURL(ServletRequest request) {
        String[] parts = ((HttpServletRequest) request).getServletPath().split("/");
        if (parts.length < 2)
            return "";
        return ((parts[1]));
    }

}
