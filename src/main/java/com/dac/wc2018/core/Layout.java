package com.dac.wc2018.core;

import java.lang.annotation.*;

/**
 * Created by Jose on 28/03/2017.
 */

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Layout {
    String value() default "";
}

