package com.dac.wc2018.core;

import com.dac.wc2018.model.Phase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Phases {

    public static HashMap<String, Phase> phases = loadPhases();

    public static String getName(String code) {
        return phases.get(code).getName();
    }

    public static List<String> getCodes() {
        return new ArrayList<>(phases.keySet());
    }

    public static HashMap<String, Phase> loadPhases() {
        HashMap<String, Phase> map = new HashMap<>();
        List<Phase> list = new ArrayList<>();
        list.add(new Phase("group", "Group phase"));
        list.add(new Phase("round16", "Round of 16"));
        list.add(new Phase("qf", "Quarter-finals"));
        list.add(new Phase("sm", "Semi-finals"));
        list.add(new Phase("third", "Play-off for third place"));
        list.add(new Phase("final", "Final"));
        list.forEach(phase -> map.put(phase.getCode(), phase));
        return map;
    }
}
