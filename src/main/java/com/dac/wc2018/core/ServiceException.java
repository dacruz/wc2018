package com.dac.wc2018.core;

public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 4311803947101122472L;

    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(Exception e) {
        super(e);
    }

}
