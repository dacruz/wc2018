package com.dac.wc2018.core;

import com.dac.wc2018.services.DataValidationService;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecurityInterceptor extends HandlerInterceptorAdapter {

    @Inject
    private DataValidationService validation;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        this.validation.validateSession();
        return super.preHandle(request, response, handler);
    }
}
