package com.dac.wc2018.core;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ServiceResponse<T> {

    private T data;

    private List<Message> messages = new ArrayList<>();


    public ServiceResponse() {
    }

    public ServiceResponse(T data) {
        this.data = data;
    }
}
