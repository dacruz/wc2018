package com.dac.wc2018.core;

import com.dac.wc2018.model.UserBet;

public abstract class PointsCalculator {

    public static int calculateNumberOfPoints(UserBet bet) {

        if (bet == null || bet.getMatch() == null || bet.getMatch().getT1Score() == null || bet.getMatch().getT2Score() == null) {
            return 0;
        }

        int points = 0;

        //Check score t1
        if (bet.getMatch().getT1Score() == bet.getT1Score()) {
            points = points + 1;
        }

        //Check score t2
        if (bet.getMatch().getT2Score() == bet.getT2Score()) {
            points = points + 1;
        }


        //Draw same score
        if (bet.getMatch().getT1Score() == bet.getT1Score() && bet.getMatch().getT2Score() == bet.getT2Score()) {
            points = points + 2;
        }
        //Draw different score
        else if (bet.getMatch().getT1Score() == bet.getMatch().getT2Score() && bet.getT1Score() == bet.getT2Score()) {
            points = points + 2;
        }
        // Check for victory of t1
        else if (bet.getMatch().getT1Score() > bet.getMatch().getT2Score() && bet.getT1Score() > bet.getT2Score()) {
            points = points + 2;
        }
        // Check for victory of t2
        else if (bet.getMatch().getT2Score() > bet.getMatch().getT1Score() && bet.getT2Score() > bet.getT1Score()) {
            points = points + 2;
        }
        return points;
    }
}
