package com.dac.wc2018;

import com.dac.wc2018.core.AdminInterceptor;
import com.dac.wc2018.core.PathLocaleChangeInterceptor;
import com.dac.wc2018.core.SecurityInterceptor;
import com.dac.wc2018.core.ThymeleafLayoutInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

@SpringBootApplication
public class WC2018Application extends WebMvcConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(WC2018Application.class, args);
    }

    @Bean
    public LocaleResolver localeResolver() {
        return new CookieLocaleResolver();
    }

    @Bean
    public PathLocaleChangeInterceptor localeChangeInterceptor() {
        return new PathLocaleChangeInterceptor();
    }

    @Bean
    public ThymeleafLayoutInterceptor layoutInterceptor() {
        return new ThymeleafLayoutInterceptor();
    }

    @Bean
    public SecurityInterceptor securityInterceptor() {
        return new SecurityInterceptor();
    }

    @Bean
    public AdminInterceptor adminInterceptor() {
        return new AdminInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(layoutInterceptor());
        registry.addInterceptor(securityInterceptor()).addPathPatterns("/**/sec/*");
        registry.addInterceptor(adminInterceptor()).addPathPatterns("/**/admin/*");
    }
}
